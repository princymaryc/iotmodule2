## **SENSORS AND ACTUATORS**
**Sensor** converts some physical energy an electrical signal that can then be used to take a reading.A better term for a sensor is a transducer.A transducer is any physical device that converts one form of energy into another.  
**Example**  
- Temperature sensor
- Moisture sensor
- Light sensor
- Proximity sensor
- Water level sensor
- Motion sensor  
![sensor](sensor.png)

**Actuator** takes an electrical input and turns it into physical action.An actuator operates in the reverse direction of a sensor.  
**Example**
- Motors
- Relays
- Solenoid
- Linear actuators  
![actuator](actuator.png)
## **ANALOG AND DIGITAL**  
![comp](comp.png)  
**Analog and Digital signals**  
![index3](index3.png)  
### **Advantage and Disadvantage of analog signal**  
**Advantage**  
- Easy processing
- Density is much higher
- Infinite amout of data  

**Disadvantage**

- Unwanted noise in recording
- Transmitting data to long distance cause unwanted disturbance
### **Advantage and Disadvantage of Digital signal**   
**Advantage**
- The frequency of the carrier wave is usually higher
- The combined wave is transmited
- Carry more information per second than analogue signals
- Maintain quality over long distances better than analogue signals.
- They're automatic
- Easier to remove noise
- Can be very immune to noise

**Disadvantage** 
   
- Can be complex
- Output subject to quantity errors from sampling
### **MICROCONTROLLERS AND MICROPROCESSOR**
- A microcontroller is an integrated circuit (IC) device used for controlling other portions of an electronic system, usually via a microprocessor unit (MPU), memory, and some peripherals.  
- A microcontroller consists of a central processing unit (CPU), nonvolatile memory, volatile memory, peripherals, and support circuitry.  
- Microprocessor is a controlling unit of a micro-computer, fabricated on a small chip capable of performing ALU (Arithmetic Logical Unit) operations and communicating with the other devices connected to it.  
- Microprocessor consists of an ALU, register array, and a control unit. ALU performs arithmetical and logical operations on the data received from the memory or an input device. Register array consists of registers identified by letters like B, C, D, E, H, L and accumulator. 

##### **Features of Microprocessor**
- Offers built-in monitor/debugger program with interrupt capability
- Large amount of instructions each carrying out a different variation of the same operation
- Offers Parallel I/O
- Instruction cycle timer
- External memory interface   

##### **Features of Microcontroller**    
- Processor reset
- Program and Variable Memory (RAM) I/O pins
- Device clocking central processor
- Instruction cycle timers 

##### **Applications of Microprocessor**  

- Calculators
- Accounting system
- Games machine
- Complex industrial controllers
- Traffic light
- Control data
- Military applications
- Defense systems
- Computation systems  
 
 ##### **Applications of Microcontroller**  
  
- Mobile phones
- Automobiles
- CD/DVD players
- Washing machines
- Cameras
- Security alarms
- Keyboard controllers
- Microwave oven
- Watches
- Mp3 players  
### **RASPBERRY PI AND ARDUINO UNO**  
 ![index6](index6.jpeg)   
Arduino Uno is a microcontroller board based on 8-bit ATmega328P microcontroller. Along with ATmega328P, it consists other components such as crystal oscillator, serial communication, voltage regulator, etc. to support the microcontroller. Arduino Uno has 14 digital input/output pins (out of which 6 can be used as PWM outputs), 6 analog input pins, a USB connection, A Power barrel jack, an ICSP header and a reset button.  
The 14 digital input/output pins can be used as input or output pins by using pinMode(), digitalRead() and digitalWrite() functions in arduino programming.   
Each pin operate at 5V and can provide or receive a maximum of 40mA current, and has an internal pull-up resistor of 20-50 KOhms which are disconnected by default.  Out of these 14 pins, some pins have specific functions as listed below:  

- Serial Pins 0 (Rx) and 1 (Tx): Rx and Tx pins are used to receive and transmit TTL serial data. They are connected with the corresponding ATmega328P USB to TTL serial chip.  
- External Interrupt Pins 2 and 3: These pins can be configured to trigger an interrupt on a low value, a rising or falling edge, or a change in value.  
- PWM Pins 3, 5, 6, 9 and 11: These pins provide an 8-bit PWM output by using analogWrite() function.  
- SPI Pins 10 (SS), 11 (MOSI), 12 (MISO) and 13 (SCK): These pins are used for SPI communication.  
- In-built LED Pin 13: This pin is connected with an built-in LED, when pin 13 is HIGH – LED is on and when pin 13 is LOW, its off.  
- Analog pin 4 (SDA) and pin 5 (SCA) also used for TWI communication using Wire library.  
- AREF: Used to provide reference voltage for analog inputs with analogReference() function.  
- Reset Pin: Making this pin LOW, resets the microcontroller.  
Along with 14 Digital pins, there are 6 analog input pins, each of which provide 10 bits of resolution, i.e. 1024 different values. They measure from 0 to 5 volts but this limit can be increased by using AREF pin with analog Reference() function. 

**RASPBERRY PI**  
![rasppi](rasppi.jpeg)  
The Raspberry Pi itself is an embedded computer, or also an SBC (single board computer). The processor on the Raspberry Pi is somewhat of a hybrid between a microprocessor and microcontroller. It is actually a SoC (system on chip), containing multiple dies stacked on top of each other, with a Broadcom BCM 2835/6/7 ARM CPU, a Broadcom VideoCore GPU (graphics processing unit) and RAM all tied together.  
Microcontrollers in general have both program and data memory inside a single chip, along with various peripherals, whereas microprocessors use external memory and peripherals (e.g. the Intel or AMD processor in your desktop or laptop).The Broadcom CPU does in fact include a host of on-chip peripherals, namely:  
- Timers
- Interrupt controller
- GPIO
- USB
- PCM / I2S
- DMA controller
- I2C’s
- SPI’s
- PWM
- UART’s  
**Memory**  
The raspberry pi model Aboard is designed with 256MB of SDRAM and model B is designed with 51MB.Raspberry pi is a small size PC compare with other PCs. The normal PCs RAM memory is available in gigabytes. But in raspberry pi board, the RAM memory is available more than 256MB or 512MB.  
**CPU (Central Processing Unit)**  
The Central processing unit is the brain of the raspberry pi board and that is responsible for carrying out the instructions of the computer through logical and mathematical operations. The raspberry pi uses ARM11 series processor. 
**GPU (Graphics Processing Unit)**  
The GPU is a specialized chip in the raspberry pi board and that is designed to speed up the operation of image calculations. This board designed with a Broadcom video core IV and it supports OpenGL.  
**Ethernet Port**  
The Ethernet port of the raspberry pi is the main gateway for communicating with additional devices. The raspberry pi Ethernet port is used  to plug your home router to access the internet.  
**GPIO Pins**  
The general purpose input & output pins are used in the raspberry pi to associate with the other electronic boards. These pins can accept input & output commands based on programming raspberry pi. The raspberry pi affords digital GPIO pins. These pins are used to connect other electronic components. For example, you can connect it to the temperature sensor to transmit digital data.  
**XBee Socket**  
The XBee socket is used in raspberry pi board for the wireless communication purpose.  
**Power Source Connector**  
The power source cable is a small switch, which  is placed on side of the shield. The main purpose of the power source connector is to enable an external power source.  
**UART**  
The Universal Asynchronous Receiver/ Transmitter is a serial input & output port. That can be used to transfer the serial data in the form of text and it is useful for converting the debugging code.  
**Display**  
The connection options of the raspberry pi board are two types such as HDMI and Composite.The O/Ps of the Raspberry Pi audio and video through HMDI, but does not support HDMI I/p. Older TVs can be connected using composite video. 
### **SERIAL AND PARALLEL COMMUNICATION**   
**Parallel Interface** 
The general purpose input & output pins are used in the raspberry pi to associate with the other electronic boards. These pins can accept input & output commands based on programming raspberry pi. The raspberry pi affords digital GPIO pins. These pins are used to connect other electronic components. For example, you can connect it to the temperature sensor to transmit digital data.
working  
Stands for "General Purpose Input/Output." GPIO is a type of pin found on an integrated circuit that does not have a specific function. ... These pins act as switches that output 3.3 volts when set to HIGH and no voltage when set to LOW. You can connect a device to specific GPIO pins and control it with a software program.   
**Serial Interface**  
Serial communication: UART, SPI, I2C, USB  
**Universal Asynchronous Receiver/Transmitter (UART)**
a basic UART system provides robust, moderate-speed, full-duplex communication with only three signals: Tx (transmitted serial data), Rx (received serial data), and ground. 
UART” is rather vague. Various aspects of the interface—number of data bits, number of stop bits, logic levels, parity—can be adapted to the needs of the system. In this article, I will focus on UART implementations that are commonly found in modern microcontroller applications.A basic UART system provides robust, moderate-speed, full-duplex communication with only three signals: Tx (transmitted serial data), Rx (received serial data), and ground. In contrast to other protocols such as SPI and I2C, no clock signal is required because the user gives the UART hardware the necessary timing information.  
**SPI**  
The Serial Peripheral Interface Bus enables full-duplex serial data transfer between multiple integrated circuits.
The Serial Peripheral Interface Bus provides full-duplex synchronous communication between a master device and a slave using four data lines.  
Basic Master-Slave Configuration  
The Serial Peripheral Interface allows bits of data to be shifted out of a master device into a slave, and at the same time, bits can be shifted out of the slave into the master.As SPI is not standardized, it is possible to encounter situations where either the Most Significant Bit (MSb) or the Least Significant Bit (LSb) is transferred first. Check the datasheet for your device and set up your data-handling routines accordingly. If you are using an Arduino, you can refer to this page for information on configuring your SPI port.  
**I2C**  
Inter–integrated circuit (AKA I2C) serial-communications protocol.  
MANY MASTER MANY SLAVE   
The point is, there are a lot of things that can go wrong in this sort of communication environment. You have to keep this in mind when you are learning about I2C because, otherwise, the protocol will seem insufferably complicated and finicky. The fact is, this extra complexity is what allows I2C to provide flexible, extensible, robust, low-pin-count serial communication.The final layer of fog settles in when you notice that SMB or SMBus is apparently used as yet another way of referring to the I2C bus. Actually, these abbreviations refer to the System Management Bus, which is distinct from, though almost identical to, the I2C bus.  
**USB**  
A straightforward, reliable communications link between a microcontroller and a PC can add powerful functionality to a wide variety of embedded applications—you could upload sensor data to a PC for analysis, send commands from a graphical user interface, or even incorporate a boot loading feature that makes it possible to update firmware without a typical debug interface. Not too long ago, the standard choice for this functionality was a computer’s “serial port,” which is an exceedingly vague term that refers to a universal asynchronous receiver/transmitter (UART) designed for RS-232 signal characteristics. Nowadays, though, using a serial port to communicate with a microcontroller is perhaps equivalent to plowing a field with a team of oxen—the oxen are simple beasts and they get the job done, but few would choose them over the latest GPS-guided mega-tractor.  









 





  


